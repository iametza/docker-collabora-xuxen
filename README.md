# Collabora Online Development Edition (CODE) + Xuxen

[iametza](https://iametza.eus)k garatutako Dockerfile honek Xuxen integratzen du [Collabora CODE](https://www.collaboraoffice.com/code/)n.

[Xuxen](http://xuxen.eus) [Elhuyar Fundazioa](https://www.elhuyar.eus/)ren eta [Euskal Herriko Unibertsitateko](https://www.ehu.eus/eu/) [Ixa taldea](http://ixa.si.ehu.es/)ren artean garatutako euskarazko zuzentzaile ortografiko eta gramatikala da.

Dockerfile-aren iturburu-kodea: https://gitlab.com/iametza/docker-collabora-xuxen (GNU GPLv3)

Docker-CODE proiektuaren Dockerfile-ean oinarritua: https://github.com/CollaboraOnline/Docker-CODE

## Ezaugarriak
 - Xuxen automatikoki deskargatu, erauzi eta kontainer barruko fitxategi-sisteman dagokion lekura kopiatzen du Collabora CODEn erabili ahal izateko.

## Irudia nola eraiki

Exekutatu Dockerfile-a dagoen karpetan:
``` docker image build -t iametza/collabora-xuxen:6.4.9.1 . ```

## Kontainerra martxan jartzeko
Irudia docker pull komandoarekin eskuratu edo aurreko pausoan azaldu bezala eraiki ondoren, exekutatu:
``` docker run -t -d -p 127.0.0.1:9980:9980 -e 'domain=azpi-domeinua\\.domeinua\\.eus' -e 'dictionaries=eu es_ES fr_FR en_GB en_US' --restart always --cap-add MKNOD iametza/collabora-xuxen:6.4.9.1 ```

### Oharrak
- *GARRANTZITSUA*: **dictionaries parametroan euskara (eu) jarri behar da. Bestela euskara ez da Collaborako hizkuntzen artean agertuko.** Euskaraz gain LibreOfficen zuzentzaile ortografikoa duten beste hizkuntzak ere gehitu daitezke: https://wiki.documentfoundation.org/Development/Dictionaries (gehienak ez ditugu probatu).
- domain parametroan Nextcloud instalatuta daukagun oinarrizko URLa jarri behar da, puntuei aurretik \\\\ jarriz (Plesk erabiltzen baduzu ez jarri).
- Irudiak beste parametro batzuk ere baditu, xehetasunetarako ikusi dokumentazioa: https://www.collaboraoffice.com/code/docker/

## Lizentziak
LibreOfficerako Xuxenek GNU GPLv2 lizentzia du: https://extensions.libreoffice.org/extensions/xuxen-5-zuzentzaile-ortografikoa

Proiektu hau [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) lizentzia duen software librea da.

<a rel="license" href="https://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License version 3" style="border-width:0" src="https://www.gnu.org/graphics/gplv3-127x51.png" /></a>
